<?php
	class Page extends CI_Controller {

		public function index(){
			echo "This is page.php";
		}
        public function view($pagename = 'home')
        {
        	if (!file_exists(APPPATH.'views/'.$pagename.'.php'))
        		show_404();

        	$data['title'] = ucfirst($pagename); // Capitalize the first letter

        	$this->load->view('header', $data);
        	$this->load->view(''.$pagename, $data);
        	$this->load->view('footer', $data);
        }
}
?>